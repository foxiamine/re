﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Localize
{
    public class LocalisedManager : MonoBehaviour
    {
        public static LocalisedManager inst;

        public delegate void OnChangeLanguage();
        public event OnChangeLanguage onChangeLanguage;
        int current = 0;
        [SerializeField]
        public List<word> words = new List<word>();
        public TextAsset[] locales;

        public static string GetTextForKey(string key)
        {
            if (inst == null || inst.words == null)
            {

            }
            foreach (word T in inst.words)
            {
                if (key == T.key)
                {
                    return T.value;
                }
            }
            return key;
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }

        [ContextMenu("load")]
        public void Load()
        {
            Load(0);
        }

        public void Load(int localNum)
        {
            string json = locales[localNum].text;
            Wrapper<word> wrap = JsonUtility.FromJson<Wrapper<word>>(json);

            words.Clear();
            words.AddRange(wrap.Items);
        }


        public void OnClickChangeLanguage()
        {
            current++;
            current = (int)Mathf.Repeat(current, locales.Length);
            Load(current);
            
            onChangeLanguage?.Invoke();
        }

        private void OnValidate()
        {
            inst = this;

        }

        private void Start()
        {
            inst = this;
            StartCoroutine(LateStart());
        }

        IEnumerator LateStart()
        {
            yield return new WaitForSeconds(.001f);
            OnClickChangeLanguage();
        }


    }

    [System.Serializable]
    public struct word
    {
        public string key;
        public string value;
    }

    public static class Extension
    {
        public static string Localised(this string key)
        {
            return LocalisedManager.GetTextForKey(key);
        }
    }
}