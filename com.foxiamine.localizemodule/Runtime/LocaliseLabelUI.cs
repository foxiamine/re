﻿using Localize;
using UnityEngine;
using UnityEngine.UI;

namespace Localize
{
    public class LocaliseLabelUI : MonoBehaviour
    {
        private string key;

        void Start()
        {
            key = GetComponent<Text>().text;
            if (Localize.LocalisedManager.inst != null) GetComponent<Text>().text = key.Localised();
        }
    }
}