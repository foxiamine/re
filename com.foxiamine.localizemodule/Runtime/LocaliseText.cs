﻿using Localize;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Localize
{
    [System.Serializable]
    public class LocaliseText
    {
        public string key = "";
        [SerializeField]
        public string value
        {
            get { return key.Localised(); }
            set { key = value; }
        }

        public LocaliseText() { }
        public LocaliseText(string Key) => key = Key;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(LocaliseText))]
public class LocaliseTextDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 55;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        Rect amountRect = new Rect(position.x, position.y, position.width, 18);
        Rect unitRect = new Rect(position.x, position.y+18, position.width, 38);

        var key = property.FindPropertyRelative("key");

        EditorGUI.PropertyField(amountRect, key);
        EditorGUI.LabelField(unitRect, key.stringValue.Localised(), GUI.skin.GetStyle("HelpBox"));

        //

        EditorGUI.EndProperty();
    }
}

#endif
